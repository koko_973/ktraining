﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using KTreining.Model;
using KTraining.Models;

namespace KTraining.Controllers
{
    [Authorize]
    public class RequestController : BaseController
    {
        //POST: Send request to join in course
        [HttpPost]
        [Authorize(Roles = "Student")]
        public ActionResult SendRequestToJoin(int id)
        {
            var student = this.studentService.GetStudentByAppUserId(this.User.Identity.GetUserId());
            this.requestToJoinService.Add(new RequestToJoin
            {
                CourseId = id,
                SendById = student.Id
            });
            var course = courseService.GetById(id);
            this.notificationService.Add(new Notification
            {
                UserId = course.Teacher.ApplicationUserId,
                Content = "Имате една нова заява за участиве във ваш курс. ",
                Link = "/Request/RequestsToJoin"

            });
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //GET:Get teacher's requests to joining in course
        [HttpGet]
        [Authorize(Roles = "Teacher")]
        public ActionResult RequestsToJoin()
        {
            var teacher = this.userService.GetTeacherByAppUserId(this.User.Identity.GetUserId());
            var requests = this.requestToJoinService.GetRequestsForTeacher(teacher.Id)
                .OrderBy(x => x.CourseId)
                .ToList()
                .ConvertAll(
                x => new RequestToJoinViewModel
                {
                    Course = x.Course,
                    Id = x.Id,
                    SendBy = x.SendBy
                });

            return View(requests);
        }

        [HttpGet]
        [Authorize(Roles = "Teacher")]
        public JsonResult CountRequestsToJoin()
        {
            var teacher = this.userService.GetTeacherByAppUserId(this.User.Identity.GetUserId());
            var requests = this.requestToJoinService.GetRequestsForTeacher(teacher.Id)
                .OrderBy(x => x.CourseId)
                .ToList();
            return Json(requests.Count, JsonRequestBehavior.AllowGet);
        }

        //POST: Accept request
        [HttpPost]
        [Authorize(Roles = "Teacher")]
        [ValidateAntiForgeryToken]
        public ActionResult AcceptRequest(int id)
        {
            var request = this.requestToJoinService.GetById(id);
            this.courseService.AddStudentToCourse(request.CourseId, request.SendById);
            this.postService.Add(new Post
            {
                Content = this.User.Identity.Name + " добави " + request.SendBy.ApplicationUser.UserName + " към курса.",
                CourseId = request.CourseId,
                UserId = this.User.Identity.GetUserId(),
                Date = DateTime.Now

            });
            this.notificationService.Add(new Notification
            {
                UserId = request.SendBy.ApplicationUserId,
                Content = "Бяхте приет в курса " + request.Course.Name,
                Link = "/CourseDetails/Details/" + request.CourseId

            });
            this.requestToJoinService.Remove(request.Id);
            return RedirectToAction("RequestsToJoin");
        }

        //POST: Decline request
        [HttpPost]
        [Authorize(Roles = "Teacher")]
        [ValidateAntiForgeryToken]
        public ActionResult DeclineRequest(int id)
        {
            this.requestToJoinService.Remove(id);
            return RedirectToAction("RequestsToJoin");
        }
    }
}