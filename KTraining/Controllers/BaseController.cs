﻿using KTraining.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KTraining.Controllers
{
    public class BaseController:Controller
    {
        public IUserService userService;
        public CloudinaryService cloudinaryService;
        public ICourseService courseService;
        public IStudentService studentService;
        public IPostService postService;
        public IRequestToJoinService requestToJoinService;
        public ICloudFileService cloudFilesService;
        public ICourseImageService courseImageService;
        public IVideoService videoService;
        public ITopicService topicService;
        public ICloseQuestionService closeQuestionService;
        public IImageService imageService;
        public ICloseAnswerService closeAnswerService;
        public IOpenQuestionService openQuestionService;
        public IAutomaticTestService automaticTestService;
        public IManualTestService manualTestService;
        public ITestService testService;
        public ISolvedAutomaticTestService solvedAutomaticTestService;
        public ISolvedCloseQuestionService solvedCloseQuestionService;
        public IAutomaticTestForSolvingService autoTestForSolvingService;
        public IMarkService markService;
        public IManualTestForSolving manualTestForSolvingService;
        public ISolvedManualTestService solvedManualTestService;
        public ISolvedOpenQuestionService solvedOpenQuestionService;
        public AlphabetFuncions alphabetFunctions;
        public IStudentCompletedCourse studentCompletedCoursesService;
        public ImportQuestionService importQuestionService;
        public ExportTestService exportTestService;
        public INotificationService notificationService;

        public BaseController()
        {
            this.userService = new UserService();
            this.cloudinaryService = new CloudinaryService("onlinesystemtesting", "198959495156847", "xaESFiOp5pYOqH4EbQOs_dhnaiY");
            this.courseService = new CourseService();
            this.studentService = new StudentService();
            this.postService = new PostService();
            this.requestToJoinService = new RequestToJoinService();
            this.cloudFilesService = new CloudFileService();
            this.courseImageService = new CourseImageService();
            this.videoService = new VideoService();
            this.topicService = new TopicService();
            this.closeQuestionService = new CloseQuestionService();
            this.imageService = new ImageService();
            this.closeAnswerService = new CloseAnswerService();
            this.openQuestionService = new OpenQuestionService();
            this.automaticTestService = new AutomaticTestService();
            this.manualTestService = new ManualTestService();
            this.testService = new TestService();
            this.solvedAutomaticTestService = new SolvedAutomaticTestService();
            this.solvedCloseQuestionService = new SolvedCloseQuestionService();
            this.autoTestForSolvingService = new AutomaticTestForSolvingService();
            this.markService = new MarkService();
            this.manualTestForSolvingService = new ManualTestForSolvingService();
            this.solvedManualTestService = new SolvedManualTestService();
            this.solvedOpenQuestionService = new SolvedOpenQuestionService();
            this.alphabetFunctions = new AlphabetFuncions();
            this.studentCompletedCoursesService = new StudentCompletedCourseService();
            this.importQuestionService = new ImportQuestionService();
            this.exportTestService = new ExportTestService();
            this.notificationService = new NotificationService();
        }
    }
}