﻿using KTraining.Models;
using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace KTraining.Controllers
{
    [Authorize(Roles = "Teacher,Student")]
    public class PostController : BaseController
    {
        //GET: Get course's posts
        [HttpGet]
        public ActionResult Posts(int id)
        {
            var course = this.courseService.GetById(id);
            var viewModel = new CoursePostsViewModel
            {
                Name = course.Name,
                Id = course.Id,
                Posts = course.Posts.Reverse().ToList()
            };
            return View(viewModel);
        }

        //POST: Add post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPost(PostAddViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("/Post/Posts/" + model.CourseId);
            }
            this.postService.Add(new Post
                {
                    Content = model.Content,
                    CourseId = model.CourseId,
                    Date = DateTime.Now,
                    UserId = this.User.Identity.GetUserId()
                });
            return Redirect("/Post/Posts/" + model.CourseId);
        }
    }
}