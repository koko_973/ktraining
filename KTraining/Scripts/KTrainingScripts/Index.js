﻿$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "/Course/LatestCourses",
        traditional: true,
        success: function (data) {
            $("#latest-courses").html(unescape(data));

        }
    });

    $.ajax({
        type: "GET",
        url: "/User/FirstUserName",
        traditional: true,
        success: function (data) {
            $(".hello").html("Здравейте, " +unescape(data));

        }
    });
    $.ajax({
        type: "GET",
        url: "/Course/MostFamous",
        traditional: true,
        success: function (data) {
            $("#most-famous-courses").html(unescape(data));

        }
    });
    $.ajax({
        type: "GET",
        url: "/Notification/CountNotifications",
        traditional: true,
        success: function (data) {
            if (parseInt(data) > 0) {
                var content = "Имате " + unescape(data) + " нови известия";
                $(".nottf").html('<a href=/Notification>' + content + '<\a>');
            }

        }
    });
});