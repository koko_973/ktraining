﻿$(document).ready(function () {
    $("#input-4").fileinput({ showCaption: false, showUpload: false, browseLabel: "Избери", removeLabel: "Премахни" });
    $(".input-5").fileinput({ showCaption: false, showUpload: false, browseLabel: "Избери", removeLabel: "Премахни" });

    $(document).ready(function () {
        tinymce.init({
            language: "bg_BG",
            selector: "textarea",
            menubar: false,
            statusbar: false,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
        });
    });

    function initMCEexact(e) {
        tinyMCE.init({
            mode: "exact",
            elements: e
        });
    }
    $("#addAnswer").click(function () {
        var count = $(".answer").length;
        var newHtml = $(".answer").first()[0].outerHTML;
        var res = replaceAll('[0]', '[' + count + ']', newHtml);
        res = replaceAll('_0_', '_' + count + '_', res);
        $("#answers").append(res);
        initMCEexact($("textarea").last().attr("id"));
        tinyMCE.triggerSave();
        $(".input-5").last().parent().parent().remove();
        var html = '<input class="input-5" formenctype="multipart/form-data" id="Answers_' + count + '__Images" multiple="multiple" name="Answers[' + count + '].Images" type="file" value="System.Web.HttpPostedFileBase[]">';
        $(".answer").children(".form-group").last().children('.col-md-10').first().append(html);
        $(".input-5").last().fileinput({ showCaption: false, showUpload: false, browseLabel: "Избери", removeLabel: "Премахни" });

    });

    function replaceAll(find, replace, str) {
        while (str.indexOf(find) > -1) {
            str = str.replace(find, replace);
        }
        return str;
    }
});