﻿$(document).ready(function () {

    function Mark() {
        var points = 0;
        var maxP = parseFloat($(".maxPoints").val());
        var rate = parseFloat($(".rate").val());
        $(".test-point").each(function (index) {
            if (!IsNumeric($(this).val()) )
            {
                $(this).val(0);
            }
            points += parseFloat($(this).val());
        });
        htm = ((parseFloat(points) - parseFloat(rate) * parseFloat(maxP) / 100) * 3) / (parseFloat(maxP) - parseFloat(rate) * parseFloat(maxP) / 100) + parseFloat(3);
        if (htm > 6) {
            htm = 6;
        }
        if (htm < 2) {
            htm = 2;
        }
        var multiplier = Math.pow(10, 2);
        $(".markS").html("Оценка: " + parseFloat(htm).toFixed(2));
        $("#mark-hidden").val(parseFloat(htm).toFixed(2));
    }
    function IsNumeric(input) {
        var RE = /^-{0,1}\d*\.{0,1}\d+$/;
        return (RE.test(input));
    }
    Mark();
    $(".test-point").focusout(function () {
        if (isNaN(t))
        {
            $(this).val(0);
        }
    });
    $(".test-point").keyup(function () {
        var t = parseFloat($(this).val());
        var m = parseFloat($(this).attr("max"));
        if (isNaN(t))
        {
            return;
        }
        if (t > m) {
            $(this).val($(this).attr("max"));
        }
        if ($(this).val() < 0) {
            $(this).val(0);
        }
        Mark();
    });
});