﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KTraining.Models
{
    public class OpenQuestionFullViewModel
    {

        public List<Image> Images { get; set; }

        public int Id { get; set; }

        [Required]
        [Display(Name="Въпрос")]
        [AllowHtml]
        public string Content { get; set; }

        public virtual Topic Topic { get; set; }

        [Display(Name="Точки")]
        public double Points { get; set; }

        [Url]
        [Display(Name="Помощ")]
        public string HelpLink { get; set; }
    }

    public class OpenQuestionsViewModel
    {
        public int TopicId { get; set; }
        public List<OpenQuestionViewModel> Questions { get; set; }
    }

    public class OpenQuestionViewModel
    {
        public int Id { get; set; }

        public string Content { get; set; }
    }

    public class AddOpenQuestionViewModel
    {
        [Required(ErrorMessage="Полето въпрос е задължително")]
        [AllowHtml]
        [Display(Name="Въпрос")]
        public string Content { get; set; }

        public int TopicId { get; set; }

        [Required(ErrorMessage = "Полето точки е задължително")]
        [Display(Name="Точки")]
        public double Points { get; set; }

        [Display(Name="Снимки")]
        public HttpPostedFileBase[] Images { get; set; }

        [Url]
        [Display(Name="Помощ")]
        public string HelpLink { get; set; }
    }

    public class SelectOpenQuestionViewModel
    {
        public OpenQuestion Question { get; set; }

        public bool IsSelected { get; set; }
    }

    public class SolveOpenQuestionViewModel
    {
        [Required]
        public int QuestionId { get; set; }

        public string Content { get; set; }

        public int Index { get; set; }

        public ICollection<Image> Images { get; set; }

        [Required]
        public int SolvedTestId { get; set; }

        [Required]
        public int SolvedQuestionId { get; set; }

        [AllowHtml]
        public string Answer { get; set; }
    }
}