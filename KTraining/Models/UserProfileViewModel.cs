﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KTraining.Models
{
    public class UserProfileViewModel
    {
        [Required(ErrorMessage="Полето Имейл е задължително !")]
        [EmailAddress]
        [Display(Name="Имейл")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Полето Първо име е задължително !")]
        [Display(Name = "Първо име")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Полето Бащино име е задължително !")]
        [Display(Name = "Бащино име")]
        public string SecondName { get; set; }

        [Required(ErrorMessage = "Полето Фамилия е задължително !")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Държава")]
        public string Country { get; set; }

        [Display(Name = "Град")]
        public string City { get; set; }

        [Display(Name = "За мен")]
        [AllowHtml]
        public string AboutMe { get; set; }

        [Display(Name = "Телефонен номер")]
        [Phone]
        public string PhoneNumber { get; set; }

        [Display(Name = "Скайп")]
        public string Skype { get; set; }

        [Required]
        public string Id { get; set; }

        [Display(Name = "Роля")]
        public string Role { get; set; }

        public List<CourseViewModel> CompleteCourses { get; set; }
    }

    public class UserSearchViewModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }
    }
}