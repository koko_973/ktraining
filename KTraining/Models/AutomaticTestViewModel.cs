﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KTraining.Models
{
    public class ShowAutomaticTestViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public int Rate { get; set; }

        [Required]
        public int Time { get; set; }

        public List<CloseQuestion> CloseQuestions { get; set; }
    }

    public class AddRandomAutomaticTestViewModel
    {
        [Required(ErrorMessage="Полето Заглавие е задължително")]
        [Display(Name="Заглавие")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Полето Време е задължително")]
        [Display(Name = "Време")]
        public int Time { get; set; }

        [Required(ErrorMessage = "Полето Оценяване е задължително")]
        [Display(Name = "Оценяване (%)")]
        public int Rate { get; set; }

        public List<SelectTopicAutomaticRandViewModel> SelectedTopics { get; set; }
    }

    public class AddSimpleAutomaticTestViewModel
    {
        [Required(ErrorMessage = "Полето Заглавие е задължително")]
        [Display(Name = "Заглавие")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Полето Време е задължително")]
        [Display(Name = "Оценяване (%)")]
        public int Rate { get; set; }

        [Required(ErrorMessage = "Полето Оценяване е задължително")]
        [Display(Name = "Време")]
        public int Time { get; set; }

        public List<TopicQuestionsAutomaticViewModel> TopicQuestions { get; set; }
    }

    public class StartAutoTestViewModel
    {
        public string TeacherName { get; set; }

        public string Title { get; set; }

        public int Id { get; set; }

        public int Time { get; set; }

        public int SolvedTestId { get; set; }

        public int QuestionCount { get; set; }
    }

    public class AddQuestionToAutoTest
    {
        [Required]
        public int TestId { get; set; }

        public List<TopicQuestionsAutomaticViewModel> Topics { get; set; }
    }
}