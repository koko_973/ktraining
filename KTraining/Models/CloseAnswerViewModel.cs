﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KTraining.Models
{
    public class AddCloseAnswerViewModel
    {
        [Required(ErrorMessage="Полето Отговор е задължително")]
        [AllowHtml]
        [Display(Name = "Отговор")]
        public string Content { get; set; }

        [Required]
        [Display(Name = "Правилен")]        
        public bool Correct { get; set; }

        [Required]
        public int QuestionId { get; set; }
        [Display(Name="Снимки")]
        public HttpPostedFileBase[] Images { get; set; }
    }

    public class UpdateCloseAnswerViewModel
    {
        [Required]
        public int AnswerId { get; set; }

        [Required]
        [AllowHtml]
        [Display(Name="Отговор")]
        public string Content { get; set; }

        [Required]
        [Display(Name="Правилен")]
        public bool Correct { get; set; }

        [Required]
        public int QuestionId { get; set; }
    }
}