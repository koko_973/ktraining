﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IVideoService
    {
        void Add(Video model);
        ICollection<Video> GetVideosForCourse(int courseId);
        Video GetById(int id);
        void Delete(int id);
    }

    public class VideoService : BaseService, IVideoService
    {
        public void Add(Video model)
        {
            this.context.Videos.Add(model);
            this.context.SaveChanges();
        }

        public ICollection<Video> GetVideosForCourse(int courseId)
        {
            return this.context.Videos.Where(x => x.CourseId == courseId).ToList();
        }

        public Video GetById(int id)
        {
            return this.context.Videos.Find(id);
        }

        public void Delete(int id)
        {
            var video = GetById(id);
            this.context.Videos.Remove(video);
            this.context.SaveChanges();
        }
    }
}
