﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IAutomaticTestForSolvingService
    {
        AutomaticTestForSolving GetById(int id);
        int CountTestWithId(int id);
    }

    public class AutomaticTestForSolvingService : BaseService, IAutomaticTestForSolvingService
    {
        public AutomaticTestForSolving GetById(int id)
        {
            return this.context.AutomaticTestsForSolving.Find(id);
        }


        public int CountTestWithId(int id)
        {
            return this.context.AutomaticTestsForSolving.Where(x => x.TestId == id).Count();
        }
    }
}
