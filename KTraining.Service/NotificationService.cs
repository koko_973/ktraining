﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface INotificationService
    {
        ICollection<Notification> GetForUser(string id);
        void Add(Notification model);
        void SetSeen(string userId);
    }

    public class NotificationService : BaseService, INotificationService
    {
        public ICollection<Notification> GetForUser(string id)
        {
            return this.context.Notifications.Where(x => x.UserId == id).ToList();
        }

        public void Add(Notification model)
        {
            this.context.Notifications.Add(model);
            this.context.SaveChanges();
        }


        public void SetSeen(string userId)
        {
            var notifications = this.context.Notifications.Where(x => x.UserId == userId);
            foreach (var item in notifications)
            {
                item.Seen = true;
            }
            this.context.SaveChanges();
        }
    }
}
