﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IOpenQuestionService
    {
        int Add(OpenQuestion model);
        ICollection<OpenQuestion> GetForTopic(int topicId);
        void AddImage(int questionId, int imageId);
        OpenQuestion GetById(int id);
        void Update(OpenQuestion model);
        void Delete(int id);
    }

    public class OpenQuestionService : BaseService, IOpenQuestionService
    {
        public int Add(OpenQuestion model)
        {
            this.context.OpenQuestions.Add(model);
            this.context.SaveChanges();
            return model.Id;
        }

        public ICollection<OpenQuestion> GetForTopic(int topicId)
        {
            return this.context.OpenQuestions.Where(x => x.TopicId == topicId).ToList();
        }


        public void AddImage(int questionId, int imageId)
        {
            var question = this.context.OpenQuestions.Find(questionId);
            var image = this.context.Images.Find(imageId);
            question.Images.Add(image);
            this.context.SaveChanges();
        }

        public OpenQuestion GetById(int id)
        {
            return this.context.OpenQuestions.Find(id);
        }


        public void Update(OpenQuestion model)
        {
            var question = this.context.OpenQuestions.Find(model.Id);
            question.Content = model.Content;
            question.Points = model.Points;
            this.context.SaveChanges();
        }


        public void Delete(int id)
        {
            var question = this.context.OpenQuestions.Find(id);
            while (question.Images.Count > 0)
            {
                this.cloudinaryService.DeleteImage(question.Images.First().Source.Substring(0, question.Images.First().Source.IndexOf(".")));
                this.context.Images.Remove(question.Images.First());
            }
            this.context.OpenQuestions.Remove(question);
            this.context.SaveChanges();
        }
    }
}
