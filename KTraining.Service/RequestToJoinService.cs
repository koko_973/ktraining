﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IRequestToJoinService
    {
        void Add(RequestToJoin model);
        bool IfStudentSendRequestToCourse(int courseId, int studentId);
        ICollection<RequestToJoin> GetRequestsForTeacher(int teacherId);
        RequestToJoin GetById(int id);
        void Remove(int id);
    }

    public class RequestToJoinService : BaseService, IRequestToJoinService
    {
        public void Add(RequestToJoin model)
        {
            this.context.RequestsToJoin.Add(model);
            this.context.SaveChanges();
        }


        public bool IfStudentSendRequestToCourse(int courseId, int studentId)
        {
            return this.context.RequestsToJoin.Any(x => x.CourseId == courseId && x.SendById == studentId);
        }


        public ICollection<RequestToJoin> GetRequestsForTeacher(int teacherId)
        {
            return this.context.RequestsToJoin.Where(x => x.Course.TeacherId == teacherId).ToList();
        }


        public RequestToJoin GetById(int id)
        {
            return this.context.RequestsToJoin.Find(id);
        }


        public void Remove(int id)
        {
            var request = this.context.RequestsToJoin.Find(id);
            this.context.RequestsToJoin.Remove(request);
            this.context.SaveChanges();
        }
    }
}
