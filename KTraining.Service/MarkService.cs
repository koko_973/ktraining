﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IMarkService
    {
        void Add(Mark model);
        ICollection<Mark> MarksForStudent(int id);
        void SetSeenStudentMarks(int studentId);
        int GetUnseenMarks(int studentId);
    }
    public class MarkService : BaseService, IMarkService
    {
        public void Add(Mark model)
        {
            model.Seen = false;
            this.context.Marks.Add(model);
            this.context.SaveChanges();
        }

        public ICollection<Mark> MarksForStudent(int id)
        {
            return this.context.Marks.Where(x => x.StudentId == id).ToList();
        }

        public void SetSeenStudentMarks(int studentId)
        {
            var marks = this.context.Marks.Where(x => x.StudentId == studentId);
            foreach (var item in marks)
            {
                item.Seen = true;
            }
            this.context.SaveChanges();
        }


        public int GetUnseenMarks(int studentId)
        {
            return this.context.Marks.Where(x => x.StudentId == studentId && x.Seen == false).Count();
        }
    }
}
