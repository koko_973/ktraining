﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface ICloseAnswerService
    {
        int Add(CloseAnswer model);
        void AddImage(int answerId, int imageId);
        void Update(CloseAnswer model);
        CloseAnswer GetById(int id);
        void Delete(int id);
    }

    public class CloseAnswerService : BaseService, ICloseAnswerService
    {
        public int Add(CloseAnswer model)
        {
            this.context.CloseAnswers.Add(model);
            this.context.SaveChanges();
            return model.Id;
        }

        public void AddImage(int answerId, int imageId)
        {
            var image = this.context.Images.Find(imageId);
            this.context.CloseAnswers.Find(answerId).Images.Add(image);
            this.context.SaveChanges();
        }


        public void Update(CloseAnswer model)
        {
            var answer = this.context.CloseAnswers.Find(model.Id);
            answer.Content = model.Content;
            answer.Correct = model.Correct;
            this.context.SaveChanges();
        }


        public CloseAnswer GetById(int id)
        {
            return this.context.CloseAnswers.Find(id);
        }


        public void Delete(int id)
        {
            var answer = this.context.CloseAnswers.Find(id);
            while (answer.Images.Count > 0)
            {
                this.cloudinaryService.DeleteImage(answer.Images.First().Source.Substring(0, answer.Images.First().Source.IndexOf('.')));
                this.context.Images.Remove(answer.Images.First());
            }
            this.context.CloseAnswers.Remove(answer);
            this.context.SaveChanges();
        }
    }
}
