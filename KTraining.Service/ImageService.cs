﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IImageService
    {
        int Add(Image model);
        void Delete(int id);
    }

    public class ImageService : BaseService, IImageService
    {
        public int Add(Image model)
        {
            this.context.Images.Add(model);
            this.context.SaveChanges();
            return model.Id;
        }

        public void Delete(int id)
        {
            var image = this.context.Images.Find(id);
            this.cloudinaryService.DeleteImage(image.Source.Substring(0, image.Source.IndexOf('.')));
            this.context.Images.Remove(image);
            this.context.SaveChanges();
        }
    }
}
