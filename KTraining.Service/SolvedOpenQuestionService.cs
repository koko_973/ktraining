﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface ISolvedOpenQuestionService
    {
        void AddAnswer(int solvedQuestionId, string content);
        SolvedOpenQuestion GetOpenQuestionByManualTestAndIndex(int testId, int questionIndex);
        void SetPoints(int questionId, double points);
    }

    public class SolvedOpenQuestionService : BaseService, ISolvedOpenQuestionService
    {
        public void AddAnswer(int solvedQuestionId, string content)
        {
            this.context.SolvedOpenQuestions.Find(solvedQuestionId).Answer = content;
            this.context.SaveChanges();
        }

        public SolvedOpenQuestion GetOpenQuestionByManualTestAndIndex(int testId, int questionIndex)
        {
            return this.context.SolvedManualTests.Find(testId).SolvedOpenQuestions.ElementAt(questionIndex);
        }


        public void SetPoints(int questionId, double points)
        {
            this.context.SolvedOpenQuestions.Find(questionId).Points = points;
            this.context.SaveChanges();
        }
    }
}
