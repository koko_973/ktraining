﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface ISolvedCloseQuestionService
    {
        void AddSelectedAnswer(List<int> selectedAnswers, int solvedQuestionId);
        SolvedCloseQuestion GetQuestionByAutoTestAndIndex(int testId, int questionIndex);
        SolvedCloseQuestion GetCloseQuestionByManualTestAndIndex(int testId, int questionIndex);
    }

    public class SolvedCloseQuestionService : BaseService, ISolvedCloseQuestionService
    {
        public void AddSelectedAnswer(List<int> selectedAnswers, int solvedQuestionId)
        {
            var solvedQ = this.context.SolvedCloseQuestions.Find(solvedQuestionId);
            foreach (var item in selectedAnswers)
            {
                var answer = this.context.CloseAnswers.Find(item);
                solvedQ.SelectedAnswers.Add(answer);
            }
            this.context.SaveChanges();
        }

        public SolvedCloseQuestion GetQuestionByAutoTestAndIndex(int testId, int questionIndex)
        {
            return this.context.SolvedAutomaticTests.Find(testId).SolvedQuestions.ElementAt(questionIndex);
        }


        public SolvedCloseQuestion GetCloseQuestionByManualTestAndIndex(int testId, int questionIndex)
        {
            return this.context.SolvedManualTests.Find(testId).SolvedCloseQuestions.ElementAt(questionIndex);
        }

    }
}
