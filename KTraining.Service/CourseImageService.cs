﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface ICourseImageService
    {
        void Add(CourseImage model);
        void Delete(int id);
        ICollection<CourseImage> GetImagesForCourse(int courseId);
        CourseImage GetById(int id);
    }

    public class CourseImageService : BaseService, ICourseImageService
    {
        public void Add(CourseImage model)
        {
            this.context.CourseImages.Add(model);
            this.context.SaveChanges();
        }

        public void Delete(int id)
        {
            var image = this.context.CourseImages.Find(id);
            this.cloudinaryService.DeleteImage(image.Source.Substring(0, image.Source.IndexOf(".")));
            this.context.CourseImages.Remove(image);
            this.context.SaveChanges();
        }

        public ICollection<CourseImage> GetImagesForCourse(int courseId)
        {
            return this.context.CourseImages.Where(x => x.CourseId == courseId).ToList();
        }


        public CourseImage GetById(int id)
        {
            return this.context.CourseImages.Find(id);
        }
    }
}
