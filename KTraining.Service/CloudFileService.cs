﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface ICloudFileService
    {
        void Add(CloudFile model);
        CloudFile GetByName(string name);
        CloudFile GetById(int id);
        void Delete(int id);
    }

    public class CloudFileService : BaseService, ICloudFileService
    {
        public void Add(CloudFile model)
        {
            this.context.CloudFiles.Add(model);
            this.context.SaveChanges();
        }

        public CloudFile GetByName(string name)
        {
            return this.context.CloudFiles.Where(x => x.Source == name).First();
        }

        public void Delete(int id)
        {
            var file = this.context.CloudFiles.Find(id);
            this.cloudinaryService.DeleteFile(file.Source);
            this.context.CloudFiles.Remove(file);
            this.context.SaveChanges();
        }

        public CloudFile GetById(int id)
        {
            return this.context.CloudFiles.Find(id);
        }
    }
}
