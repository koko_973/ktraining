﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IManualTestService
    {
        int Add(ManualTest model);
        ICollection<ManualTest> GetForTeacher(int id);
        void AddCloseQuestion(int questionId, int testId);
        void AddOpenQuestion(int questionId, int testId);
        void Delete(int id);
        ManualTest GetById(int id);
        double MaxPoints(int id);
        void RemoveCloseQuestion(int questionId, int testId);
        void RemoveOpenQuestion(int questionId, int testId);
    }
    public class ManualTestService : BaseService, IManualTestService
    {
        public int Add(ManualTest model)
        {

            if (this.context.ManualTests.Where(x => x.Title == model.Title).FirstOrDefault() != null)
            {
                int index = 1;
                while (true)
                {
                    if (this.context.ManualTests.Where(x => x.Title == (model.Title + index.ToString())).FirstOrDefault() == null)
                    {
                        break;
                    }
                    index++;
                }
                model.Title = model.Title + index.ToString();
            }
            this.context.ManualTests.Add(model);
            this.context.SaveChanges();
            return model.Id;
        }

        public ICollection<ManualTest> GetForTeacher(int id)
        {
            return this.context.ManualTests.Where(x => x.TeacherId == id).ToList();
        }

        public void AddCloseQuestion(int questionId, int testId)
        {
            var test = this.context.ManualTests.Find(testId);
            var question = this.context.CloseQuestions.Find(questionId);
            test.CloseQuestions.Add(question);
            this.context.SaveChanges();
        }

        public void AddOpenQuestion(int questionId, int testId)
        {
            var test = this.context.ManualTests.Find(testId);
            var question = this.context.OpenQuestions.Find(questionId);
            test.OpenQuestions.Add(question);
            this.context.SaveChanges();
        }

        public void Delete(int id)
        {
            var test = this.context.ManualTests.Find(id);
            while (test.CloseQuestions.Count > 0)
            {
                test.CloseQuestions.Remove(test.CloseQuestions.First());
            }
            while (test.OpenQuestions.Count > 0)
            {
                test.OpenQuestions.Remove(test.OpenQuestions.First());
            }
            this.context.SaveChanges();
            this.context.ManualTests.Remove(test);
            this.context.SaveChanges();
        }


        public ManualTest GetById(int id)
        {
            return this.context.ManualTests.Find(id);
        }

        public double MaxPoints(int id)
        {
            var test = this.context.ManualTests.Find(id);
            double maxPoints = 0;
            foreach (var item in test.CloseQuestions)
            {
                maxPoints += item.Points;
            }
            foreach (var item in test.OpenQuestions)
            {
                maxPoints += item.Points;
            }
            return maxPoints;
        }


        public void RemoveCloseQuestion(int questionId, int testId)
        {
            var test = this.context.ManualTests.Find(testId);
            test.CloseQuestions.Remove(test.CloseQuestions.Where(x => x.Id == questionId).FirstOrDefault());
            this.context.SaveChanges();
        }


        public void RemoveOpenQuestion(int questionId, int testId)
        {
            var test = this.context.ManualTests.Find(testId);
            test.OpenQuestions.Remove(test.OpenQuestions.Where(x => x.Id == questionId).FirstOrDefault());
            this.context.SaveChanges();
        }
    }
}
