﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IStudentService
    {
        Student GetById(int id);
        bool ExistStudent(string studentName);
        Student GetStudentByEmail(string email);
        Student GetStudentByAppUserId(string id);
        Student GetStudentByUsername(string username);
        ICollection<Student> GetStudentsWithoutCourse(int courseId);
        void AddTestToSolve(int studentId, int testId, string testType, int courseId);
        void RemoveAutoTestToSolve(int studentId, int testId, int courseId);
        void RemoveManualTestToSolve(int studentId, int testId, int courseId);
    }

    public class StudentService : BaseService, IStudentService
    {
        public Student GetById(int id)
        {
            return this.context.Students.Find(id);
        }

        public Student GetStudentByAppUserId(string id)
        {
            return this.context.Students.Where(x => x.ApplicationUserId == id).First();
        }

        public bool ExistStudent(string studentName)
        {
            return this.context.Students.Any(x => x.ApplicationUser.UserName == studentName);
        }

        public Student GetStudentByEmail(string email)
        {
            return this.context.Students.Where(x => x.ApplicationUser.Email == email).First();
        }


        public Student GetStudentByUsername(string username)
        {
            return this.context.Students.Where(x => x.ApplicationUser.UserName == username).First();
        }


        public ICollection<Student> GetStudentsWithoutCourse(int courseId)
        {
            var studentsInCourse = this.context.Courses.Find(courseId).Students.ToList();
            var allStudents = this.context.Students.ToList();
            foreach (var item in studentsInCourse)
            {
                if (allStudents.Contains(item))
                {
                    allStudents.Remove(item);
                }
            }
            return allStudents;
        }


        public void AddTestToSolve(int studentId, int testId, string testType, int courseId)
        {
            if (testType == "auto")
            {
                var test = this.context.AutomaticTests.Find(testId);
                if (!this.context.AutomaticTestsForSolving.Any(x => x.CourseId == courseId && x.StudentId == studentId && x.TestId == testId))
                {
                    this.context.AutomaticTestsForSolving.Add(
                        new AutomaticTestForSolving
                        {
                            CourseId = courseId,
                            StudentId = studentId,
                            TestId = testId
                        });
                }
            }
            else if (testType == "manual")
            {
                var test = this.context.ManualTests.Find(testId);
                if (!this.context.ManualTestsForSolving.Any(x => x.CourseId == courseId && x.StudentId == studentId && x.TestId == testId))
                {
                    this.context.ManualTestsForSolving.Add(
                        new ManualTestForSolving
                        {
                            CourseId = courseId,
                            StudentId = studentId,
                            TestId = testId
                        });
                }
            }
            this.context.SaveChanges();
        }


        public void RemoveAutoTestToSolve(int studentId, int testId, int courseId)
        {
            var student = this.context.Students.Find(studentId);
            var test = student.AutomaticTestsToSolve.Where(x => x.Id == testId && x.CourseId == courseId).First();
            this.context.AutomaticTestsForSolving.Remove(test);
            this.context.SaveChanges();
        }


        public void RemoveManualTestToSolve(int studentId, int testId, int courseId)
        {
            var student = this.context.Students.Find(studentId);
            var test = student.ManualTestsToSolve.Where(x => x.Id == testId && x.CourseId == courseId).First();
            this.context.ManualTestsForSolving.Remove(test);
            this.context.SaveChanges();
        }
    }
}
