﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IStudentCompletedCourse
    {
        void Add(StudentCompletedCourse model);
    }

    public class StudentCompletedCourseService : BaseService, IStudentCompletedCourse
    {
        public void Add(StudentCompletedCourse model)
        {
            this.context.StudentCompletedCourses.Add(model);
            this.context.SaveChanges();
        }
    }
}
