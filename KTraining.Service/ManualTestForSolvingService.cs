﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IManualTestForSolving
    {
        ManualTestForSolving GetById(int id);
        int CountTestWithId(int id);
    }

    public class ManualTestForSolvingService : BaseService, IManualTestForSolving
    {
        public ManualTestForSolving GetById(int id)
        {
            return this.context.ManualTestsForSolving.Find(id);
        }


        public int CountTestWithId(int id)
        {
            return this.context.ManualTestsForSolving.Where(x => x.TestId == id).Count();
        }
    }
}
