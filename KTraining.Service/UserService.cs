﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IUserService
    {
        void AddAppUser(string userId, string role);
        Teacher GetTeacherByName(string name);
        Teacher GetTeacherByAppUserId(string id);
        ICollection<Teacher> GetAllTeachers();
        ApplicationUser GetAppUser(string id);
        void UpdateAppUser(ApplicationUser model);
        ICollection<ApplicationUser> GetAllAppUsers();

    }

    public class UserService : BaseService, IUserService
    {
        public void AddAppUser(string userId, string role)
        {
            if (role == "Student")
            {
                this.context.Students.Add(new Student
                {
                    ApplicationUserId = userId
                });
            }
            else if (role == "Teacher")
            {
                this.context.Teachers.Add(new Teacher
                {
                    ApplicationUserId = userId
                });
            }
            this.context.SaveChanges();
        }

        public Teacher GetTeacherByName(string name)
        {
            return this.context.Teachers.Where(x => x.ApplicationUser.UserName == name).First();
        }

        public Teacher GetTeacherByAppUserId(string id)
        {
            return this.context.Teachers.Where(x => x.ApplicationUserId == id).First();
        }

        public ICollection<Teacher> GetAllTeachers()
        {
            return this.context.Teachers.ToList();
        }

        public ApplicationUser GetAppUser(string id)
        {
            return this.context.Users.Find(id);
        }


        public void UpdateAppUser(ApplicationUser model)
        {
            var user = this.context.Users.Find(model.Id);
            user.UserName = model.Email;
            user.AboutMe = model.AboutMe;
            user.City = model.City;
            user.Country = model.Country;
            user.Email = model.Email;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.PhoneNumber = model.PhoneNumber;
            user.SecondName = model.SecondName;
            user.Skype = model.Skype;
            this.context.SaveChanges();
        }


        public ICollection<ApplicationUser> GetAllAppUsers()
        {
            return this.context.Users.ToList();
        }
    }
}
