﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using KTreining.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public class CloudinaryService
    {
        private Account account;
        private Cloudinary cloudinary;

        public CloudinaryService(string cloudName, string apiKey, string apiSecret)
        {
            this.account = new Account(cloudName, apiKey, apiSecret);
            cloudinary = new Cloudinary(this.account);
        }

        public string UploadImage(string fileName, Stream stream)
        {
            var uploadParams = new ImageUploadParams()
            {
                File = new CloudinaryDotNet.Actions.FileDescription(fileName, stream),
                UniqueFilename = true,
                Transformation = new Transformation().Quality(30)
            };
            var uploadResult = cloudinary.Upload(uploadParams);
            return uploadResult.Uri.Segments.Last().ToString();

        }


        public string UploadFile(string fileName, Stream stream)
        {
            var uploadParams = new RawUploadParams()
            {
                File = new CloudinaryDotNet.Actions.FileDescription(fileName, stream),
                UniqueFilename = true
            };

            var uploadResult = cloudinary.Upload(uploadParams, "raw");
            return uploadResult.Uri.Segments.Last().ToString();

        }

        public string GetImageUrl(string name)
        {
            return cloudinary.Api.UrlImgUp.BuildUrl(name);

        }

        public string GetFileUrl(string name)
        {


            return cloudinary.Api.Url.ResourceType("raw").Action("upload").BuildUrl(name);
        }

        public ICollection<CourseImage> AddPathToCourseImageName(ICollection<CourseImage> images)
        {
            var newImages = images.ToList().ConvertAll(
                x => new CourseImage
                {
                    Id = x.Id,
                    Source = GetImageUrl(x.Source),
                });
            return newImages;
        }

        public ICollection<Image> AddPathToQuestionImageName(ICollection<Image> images)
        {
            var newImages = images.ToList().ConvertAll(
                x => new Image
                {
                    Id = x.Id,
                    Source = GetImageUrl(x.Source)
                });
            return newImages;
        }

        public void DeleteFile(string name)
        {
            var delParams = new DelResParams()
            {
                PublicIds = new List<string>() { name },
                Invalidate = true,
                ResourceType = ResourceType.Raw
            };
            var r = this.cloudinary.DeleteResources(delParams);
        }

        public void DeleteImage(string name)
        {
            var delParams = new DelResParams()
            {
                PublicIds = new List<string>() { name },
                Invalidate = true,
                ResourceType = ResourceType.Image
            };
            var r = this.cloudinary.DeleteResources(delParams);
        }
    }
}
