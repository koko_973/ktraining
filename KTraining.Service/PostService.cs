﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface IPostService
    {
        void Add(Post model);
        ICollection<Post> GetPostsForCourse(int courseId);
    }

    public class PostService : BaseService, IPostService
    {
        public void Add(Post model)
        {
            this.context.Posts.Add(model);
            this.context.SaveChanges();
        }


        public ICollection<Post> GetPostsForCourse(int courseId)
        {
            return this.context.Posts.Where(x => x.CourseId == courseId).ToList();
        }
    }
}
