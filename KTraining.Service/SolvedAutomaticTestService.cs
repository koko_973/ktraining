﻿using KTreining.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTraining.Service
{
    public interface ISolvedAutomaticTestService
    {
        int Add(SolvedAutomaticTest model);
        SolvedAutomaticTest GetById(int id);
        double GetPointForTest(int id);
        bool IsWholeTestComplete(int testId, int courseId);
        void SetShow(List<int> testIds);
        void SetComplete(int testId);
    }

    public class SolvedAutomaticTestService : BaseService, ISolvedAutomaticTestService
    {
        public int Add(SolvedAutomaticTest model)
        {
            this.context.SolvedAutomaticTests.Add(model);
            this.context.SaveChanges();
            return model.Id;
        }

        public SolvedAutomaticTest GetById(int id)
        {
            return this.context.SolvedAutomaticTests.Find(id);
        }


        public double GetPointForTest(int id)
        {
            double points = 0;
            bool flag = true;
            var test = this.context.SolvedAutomaticTests.Find(id);
            foreach (var item in test.SolvedQuestions)
            {
                foreach (var item2 in item.CloseQuestion.Answers.Where(x => x.Correct == true))
                {
                    if (!item.SelectedAnswers.Select(x => x.Id).ToList().Contains(item2.Id))
                    {
                        flag = false;
                    }
                }
                if (flag)
                {
                    points += item.CloseQuestion.Points;
                }
                flag = true;

            }
            return points;
        }

        public bool IsWholeTestComplete(int testId, int courseId)
        {
            var courseStudentIds = this.context.Courses.Find(courseId).Students.Select(x => x.Id).ToList();
            var count = this.context.SolvedAutomaticTests.Where(x => x.TestId == testId)
                .Where(x => courseStudentIds.Contains(x.StudentId))
                .Where(x => x.IsComplete == true)
                .ToList().Count();

            if (count % courseStudentIds.Count == 0)
            {
                return true;
            }
            return false;
        }


        public void SetShow(List<int> testIds)
        {
            foreach (var item in testIds)
            {
                this.context.SolvedAutomaticTests.Find(item).Show = true;
            }
            this.context.SaveChanges();
        }


        public void SetComplete(int testId)
        {
            this.context.SolvedAutomaticTests.Find(testId).IsComplete = true;
            this.context.SaveChanges();
        }
    }
}
